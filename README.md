# my-project

> A Mpvue project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
//
npm install

// 本地开发
npm run dev
打开小程序查看

// 发布
npm run build

// 使用的插件
mpvue, sass, mpvue-entry, mpvue-router-patch, flyio.js, vuex, mpvue-weui, mpvue-toast, vuex-persistedstate
