export default {
  /**
   * 调用微信支付请求
  */

  wxPayment (params, cb) {
    wx.requestPayment({
      'timeStamp': params.timeStamp,
      'nonceStr': params.nonceStr,
      'package': params.package,
      'signType': 'MD5',
      'paySign': params.paySign,
      'success' (res) {
        if (typeof cb === 'function') {
          let params = {status: true, obj: res};
          cb(params);
        }
        // console.log(res);
      },
      'fail' (err) {
        // console.log(err);
        if (typeof cb === 'function') {
          let params = {status: false, obj: err};
          cb(params);
        }
      }
    });
  }
};
