import {NetworkAPIVersion, NetworkParamType} from './config';

export default {
  /**
   * 获取用户的openid，使用wxlogin的code去后台获取
  */
  getOpenid: {
    url: 'login/',
    method: 'get',
    apiVersion: NetworkAPIVersion.v1_version,
    params: {
      code: [NetworkParamType.string]
    }
  },

  getPaymentParams: {
    url: '/wx_prepay/?app_type=WxMini',
    method: 'post',
    apiVersion: NetworkAPIVersion.v1_version,
    params: {
      orderId: [NetworkParamType.string],
      openid: [NetworkParamType.string]
    }
  }
};
