const StorageTypeName = {
  hr_search_history: 'hr_search_history',
  openid: 'openid',
  location: 'location',
  system: 'system',
  wxUserInf: 'wxUserInf',
  userInf: 'userInf',
  address: 'address'
};

export default StorageTypeName;
