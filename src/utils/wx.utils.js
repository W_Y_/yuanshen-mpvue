
const callPhone = ({phone}) => {
  wx.makePhoneCall({
    phoneNumber: phone // 仅为示例，并非真实的电话号码
  });
};

const loading = ({title = '', mask = false, show = true}) => {
  if (show) {
    wx.showLoading({
      title: title,
      mask: mask
    });
  } else {
    wx.hideLoading();
  }
};

const toast = ({title, icon = 'none', hide, dur = 2000}) => {
  if (hide) {
    wx.hideToast();
  } else {
    wx.showToast({
      title: title,
      icon: icon,
      duration: dur
    });
  }
};

const showModal = obj => {
  return new Promise((resolve, reject) => {
    wx.showModal({
      title: obj.title,
      success (res) {
        resolve(res.confirm);
      }
    });
  });
};

const download = ({url}) => {
  return new Promise((resolve, reject) => {
    wx.downloadFile({
      url: url,
      success: (res) => {
        resolve(res.tempFilePath);
      },
      fail: (res) => {
        reject(res);
      }
    });
  });
};

const setNavTitle = text => {
  wx.setNavigationBarTitle({
    title: text
  });
};

const chooseImg = ({num = 1, sizeType = ['compressed']}) => {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      count: num,
      sizeType: sizeType,
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePaths 可以作为img标签的src属性显示图片
        resolve(res);
      }
    });
  });
};

const previewImage = ({urls}) => {
  wx.previewImage({
    urls: urls
  });
};

const setClipboardData = (text, titleParam) => {
  let showTitle = titleParam || '复制成功！';

  wx.setClipboardData({
    data: text,
    success (res) {
      toast({title: showTitle});
    }
  });
};

export default {
  callPhone,
  loading,
  toast,
  showModal,
  download,
  setNavTitle,
  chooseImg,
  previewImage,
  setClipboardData
};
