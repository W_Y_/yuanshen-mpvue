import Network from '@/network/index';
import Storage from '@/utils/wx.storage';
import StorageTypeName from '@/utils/storage.typename';

// let openidObj = Storage.get(StorageTypeName.openid);

// if (openidObj.openid) {
wx.login({
  success (res) {
    console.log(res);
    Network.base.getOpenid({}, null, 'mini-program/login/?code=' + res.code).then(res => {
      // console.log('登录返回信息', res);
      if (res.openid) {
        Storage.set(StorageTypeName.openid, res);
      }
    });
  }
});
// }
